import os
import sys
os.chdir(os.path.dirname(os.path.realpath(sys.argv[0])))
try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = 0
except ImportError:
    import tkinter.ttk as ttk
    py3 = 1

import injectrif_support

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    if sys.platform.__contains__("win") and not sys.platform.__contains__("darwin"):
        root.iconbitmap(os.getcwd()+'/icon.ico')
    top = PSV_License_Injector (root)
    injectrif_support.init(root, top)
    root.mainloop()

w = None
def create_PSV_License_Injector(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    top = PSV_License_Injector (w)
    injectrif_support.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_PSV_License_Injector():
    global w
    w.destroy()
    w = None


class PSV_License_Injector:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85' 
        _ana2color = '#d9d9d9' # X11 color: 'gray85' 
        font9 = "-family {DejaVu Sans} -size 0 -weight normal -slant "  \
            "roman -underline 0 -overstrike 0"

        top.geometry("176x121+725+272")
        top.title("PSV License Injector")



        self.Labelframe1 = LabelFrame(top)
        self.Labelframe1.place(relx=0.06, rely=0.0, relheight=0.7, relwidth=0.89)

        self.Labelframe1.configure(relief=GROOVE)
        self.Labelframe1.configure(text='''PSVLicenseInjector''')
        self.Labelframe1.configure(width=157)

        self.Button1 = Button(self.Labelframe1)
        self.Button1.place(relx=0.06, rely=0.10, height=26, width=137)
        self.Button1.configure(activebackground="#d9d9d9")
        self.Button1.configure(command=injectrif_support.rif)
        self.Button1.configure(text='''Select .RIF''')
        self.Button1.configure(width=137)

        self.Button2 = Button(self.Labelframe1)
        self.Button2.place(relx=0.06, rely=0.50, height=26, width=137)
        self.Button2.configure(activebackground="#d9d9d9")
        self.Button2.configure(command=injectrif_support.cmbackup)
        self.Button2.configure(text='''Select CMBackup''')
        self.Button2.configure(width=137)

        self.menubar = Menu(top,font=font9,bg=_bgcolor,fg=_fgcolor)
        top.configure(menu = self.menubar)



        self.Button3 = Button(top)
        self.Button3.place(relx=0.06, rely=0.74, height=26, width=157)
        self.Button3.configure(activebackground="#d9d9d9")
        self.Button3.configure(command=injectrif_support.inject)
        self.Button3.configure(text='''Inject .rif to CMBackup''')
        self.Button3.configure(width=157)






if __name__ == '__main__':
    vp_start_gui()



