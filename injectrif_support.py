import fnmatch
import os
import sys
import tkFileDialog
import tkMessageBox
import zipfile

import shutil

def zip(src, dst):
    zf = zipfile.ZipFile("%s" % (dst), "w", zipfile.ZIP_DEFLATED,allowZip64 = True)
    abs_src = os.path.abspath(src)
    for dirname, subdirs, files in os.walk(src):
        for filename in files:
            absname = os.path.abspath(os.path.join(dirname, filename))
            arcname = absname[len(abs_src) + 1:]
            zf.write(absname, arcname)
            os.remove(os.path.join(dirname, filename))
    zf.close()

def extractZip(src,dst):
    zf = zipfile.ZipFile(src)
    zf.extractall(path=dst)
    zf.close()

def walklevel(some_dir, level=1):
    some_dir = some_dir.rstrip(os.path.sep)
    assert os.path.isdir(some_dir)
    num_sep = some_dir.count(os.path.sep)
    for root, dirs, files in os.walk(some_dir):
        yield root, dirs, files
        num_sep_this = root.count(os.path.sep)
        if num_sep + level <= num_sep_this:
            del dirs[:]

try:
    from Tkinter import *
except ImportError:
    from tkinter import *

try:
    import ttk
    py3 = 0
except ImportError:
    import tkinter.ttk as ttk
    py3 = 1


def cmbackup():
    print "Opening file select dialog"
    global cmfile
    cmfile = tkFileDialog.askopenfilename(title='Select cmbackup',filetypes=[('Unsigned CMA Backup Files', '*.cmbackup')])
    try:
        print "cmbackup = "+cmfile
    except:
        ""
    sys.stdout.flush()

def inject():
    try:
        riff
        cmfile
    except:
        tkMessageBox.showerror(title="Error 001", message="One or more items is not selected")

    print "Extracting CMBackup"
    extractZip(cmfile,cmfile+"-e")
    #os.remove(cmfile)
    for root, dir, files in walklevel(cmfile+"-e/license",0):
        for items in fnmatch.filter(dir, '*'):
            global items1
            items1 = items


    for root, dir, files in walklevel(cmfile+"-e/license/"+items1,0):
        for items in fnmatch.filter(files, '*'):
            if items != "VITA_PATH.TXT":
                global items2
                items2 = items

    print "Removing old license: " + cmfile+"-e/license/"+items1+"/"+items2
    os.remove(cmfile+"-e/license/"+items1+"/"+items2)
    print "Adding new license: "+ riff
    shutil.copyfile(riff, cmfile+"-e/license/"+items1+"/"+items2)
    print "Repacking .cmbackup"
    zip(cmfile+"-e",cmfile)
    shutil.rmtree(cmfile+"-e")
    print "Done"
    tkMessageBox.showinfo(title="Success", message="Success! cmbackup was patched!")





    sys.stdout.flush()

def rif():
    print('Opening file select dialog')
    global riff
    riff = tkFileDialog.askopenfilename(title='Select license',filetypes=[('Playstation License File', '*.rif')])
    try:
        print ('rif = '+riff)
    except:
        ""
    sys.stdout.flush()

def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

def destroy_window():
    # Function which closes the window.
    global top_level
    top_level.destroy()
    top_level = None

if __name__ == '__main__':
    import injectrif
    injectrif.vp_start_gui()


